<?php

    namespace App\Http\Controllers;

    use Illuminate\Http\Request;
    use App\Parish;
    use App\Parishmembers;
    use View;

    class ParishController extends Controller
    {
        
        public function show(request $request)
        {
            $username=$request->input('username');
            $password=$request->input('password');
            
            $value = Parish::getloginData($username,$password);
            if($value){
                if( $value[0]->role == 'admin'){
                return View::make('admin')->with("value",$value[0]);
                    }
                else{
                print_r("welcome to user page");
                    }
                }
            else{
            print_r("You have to signup");
            }
            
        }

        public function addnewmember(request $request)
        {
            
            $name = $request->input('name');
            $address = $request->input('address');
            $phonenumber = $request->input('phonenumber');
            $data = Parishmembers::addnewmember($name,$address,$phonenumber);
            if($data){
                return View::make('admin')->with("data",$data);
            }
        }

    }