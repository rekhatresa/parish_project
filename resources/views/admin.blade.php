 @extends('index')
 @section('admin')
 <div class="container-fluid">
 	<div class="row adminMain">
 		<div class="col-xs-6 col-md-6 col-lg-6">
 			<h2>St. Antony's Church</h2>
 		</div>
 		<div class="col-xs-6 col-md-6 col-lg-6">
 			<div class="userName">
 			<p>Welcome {{$value->username}}</p>
 		    </div>
 		</div>
 	</div>
 	<div class="row fnButtons">
 		<ul class="buttonDiv">
 		<li><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addparishmemberForm">+ Add Parish Member</li>
 		<!-- <li><button type="button" class="btn btn-primary">List Parish Members</button></li> -->
 		</ul>
 	</div>
 	@yield('listParish')
  </div>
  <div class="modal fade" id="addparishmemberForm" tabindex="-1" role="dialog" aria-labelledby="addparishmemberFormLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      	 <h4 class="modal-title modaltitle" id="addparishmemberFormLabel">Add New Parish Member</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       </div>
      <div class="modal-body">
      	<form method="post" action="{{URL::to('/newmember')}}">
      		 @csrf
      		<div class="form-group">
      			<label for="name" class="labelStyle">Name:</label>
      			<input type="text" class="form-control" id="name" name="name" placeholder="Enter your full name">
      		</div>
      		<div class="form-group">
      			<label for="address" class="labelStyle">Address:</label>
      			<textarea class="form-control" id="address" name="address" placeholder="Enter address"></textarea> 
      		</div>
      		<div class="form-group">
      			<label for="phonenumber" class="labelStyle">Phone number:</label>
      			<input class="form-control" id="phonenumber" name="phonenumber" placeholder="Enter phone number">
      		</div>
      		<hr/>
      		<div style="float: right">
      		 <button type="submit" class="btn btn-primary">Save changes</button>
      		 <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      		</div>
      	</form>
      </div>
     
    </div>
  </div>
</div>
<script type="text/javascript">
$(function() {
    $('#addparishmemberForm').on("show.bs.modal", function (e) {
         $("#addparishmemberFormLabel").html($(e.relatedTarget).data('title'));
    });
});
  </script>
 <!--  <p>  {{$value->username}}
     {{$value->password}}
      {{$value->role}}</p>
<p>hsdsfksdvdlvd</p> -->
 @endsection