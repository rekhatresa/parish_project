<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Parishmembers extends Model
{
     public static function addnewmember($name,$address,$phonenumber){
    $value=DB::insert('insert into parishmembers (name,address,phonenumber,created_at,updated_at) values (?, ?, ?,? ,?)', [$name,$address,$phonenumber,now(),now()]);
   	if($value){
   		$data=DB::select('select name,address,phonenumber from parishmembers');
    return $data;
   	}

  }
}
