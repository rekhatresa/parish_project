@extends('index')
 @section('login')
 <div class="container-fluid">
    <div class="row">
        <div class="col-sm-6 col-xs-12 col-md-6 col-lg-6 loginParishBg">
            <div class="parishName">
                <h2>St. Antony's Church</h2>
            </div>
            <div class="parishName1">
                <p>Anakkal</p>
            </div>
            
        </div>
        <div class="col-sm-6 col-xs-12 col-md-6 col-lg-6">
            <div class="mainDiv">
                <div><p>Login</p></div>
                <div>
                <form method="post" action="{{URL::to('/parishaction')}}">
                    @csrf
  <div class="form-group">
    <label for="username">Username</label>
    <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username">
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
  </div>
  <button type="submit" class="btn btn-primary">Login</button>
  <p class="paraStyle">  Forgot Password? </p><p class="paraStyle">Sign up | </p>
</form>
</div>
            </div>
    </div>
</div>
    
</div>
 @endsection